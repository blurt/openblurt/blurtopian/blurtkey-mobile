let toggleElement;

export const setToggleElement = (elt) => {
  toggleElement = elt;
};

export const getToggleElement = () =>
  toggleElement
    .replace('Primary', 'WalletScreen')
    .replace('Tokens', 'EngineWalletScreen');
