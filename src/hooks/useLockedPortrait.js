import React from 'react';
import Orientation from 'react-native-orientation-locker';

export default (navigation) => {
  React.useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      Orientation.lockToPortrait();
      Orientation.removeAllListeners();
    });

    return unsubscribe;
  }, [navigation]);
};
