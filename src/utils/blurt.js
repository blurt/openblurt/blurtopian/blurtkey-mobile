import blurtTx from 'blurt-tx';
import api from 'api/keychain';
import {hiveEngine} from 'utils/config';

let blurt = require('@blurtopian/dblurt');
const DEFAULT_RPC = 'https://rpc.blurt.world';
//let client = new blurt.Client(DEFAULT_RPC);

const MAINNET_OFFICIAL = [
  'https://rpc.blurt.world',
  "https://blurt-rpc.saboin.com",
  "https://rpc.nerdtopia.de",
  "https://rpc.blurtlatam.com/",
  "https://blurt.ecosynthesizer.com/",
  "https://kentzz.blurt.world/",
];
let client = new blurt.Client(MAINNET_OFFICIAL, {
  timeout: 5000,
  addressPrefix: 'BLT',
  chainId: 'cd8d90f29ae273abec3eaa7731e25934c63eb654d55080caff2ebb7f5df6381f',
});

/*
let blurt = require('@blurtfoundation/blurtjs');
blurt.api.setOptions({ url: DEFAULT_RPC, useAppbaseApi: true });
*/

const getDefault = async () => {
  try {
    return (await api.get('/hive/rpc')).data.rpc;
  } catch (e) {
    return DEFAULT_RPC;
  }
};

export const setRpc = async (rpc) => {
  if (rpc === 'DEFAULT') {
    //rpc = await getDefault();
    rpc = DEFAULT_RPC;
  }
  client = new blurt.Client(rpc,{ url: rpc, useAppbaseApi: true });
  blurtTx.config.node = rpc;
};

export const getClient = () => client;

export const transfer = async (key, obj) => {
  return await broadcast(key, [['transfer', obj]]);
};

export const broadcastJson = async (key, username, id, active, json) => {
  return await broadcast(key, [
    [
      'custom_json',
      {
        required_auths: active ? [username] : [],
        required_posting_auths: !active ? [username] : [],
        json: typeof json === 'object' ? JSON.stringify(json) : json,
        id,
      },
    ],
  ]);
};

export const sendToken = async (key, username, obj) => {
  return await broadcastJson(key, username, hiveEngine.CHAIN_ID, true, {
    contractName: 'tokens',
    contractAction: 'transfer',
    contractPayload: obj,
  });
};

export const powerUp = async (key, obj) => {
  return await broadcast(key, [['transfer_to_vesting', obj]]);
};

export const powerDown = async (key, obj) => {
  return await broadcast(key, [['withdraw_vesting', obj]]);
};

export const delegate = async (key, obj) => {
  return await broadcast(key, [['delegate_vesting_shares', obj]]);
};

export const convert = async (key, obj) => {
  return await broadcast(key, [['convert', obj]]);
};

export const collateralizedConvert = async (key, obj) => {
  return await broadcast(key, [['collateralized_convert', obj]]);
};

export const depositToSavings = async (key, obj) => {
  return await broadcast(key, [['transfer_to_savings', obj]]);
};

export const withdrawFromSavings = async (key, obj) => {
  return await broadcast(key, [['transfer_from_savings', obj]]);
};

export const vote = async (key, obj) => {
  return await broadcast(key, [['vote', obj]]);
};

export const voteForWitness = async (key, obj) => {
  return await broadcast(key, [['account_witness_vote', obj]]);
};

export const setProxy = async (key, obj) => {
  return await broadcast(key, [['account_witness_proxy', obj]]);
};

export const post = async (
  key,
  {comment_options, username, parent_perm, parent_username, ...data},
) => {
  const arr = [
    [
      'comment',
      {
        ...data,
        author: username,
        parent_permlink: parent_perm,
        parent_author: parent_username,
      },
    ],
  ];
  if (comment_options && comment_options.length) {
    arr.push(['comment_options', JSON.parse(comment_options)]);
  }
  return await broadcast(key, arr);
};

export const signTx = (key, tx) => {
  const trx = new blurtTx.Transaction(tx);
  const signed = trx.sign(blurtTx.PrivateKey.from(key));
  return signed;
};

export const updateProposalVote = async (key, obj) => {
  return await broadcast(key, [['update_proposal_votes', obj]]);
};

export const claimRewards = async (key, obj) => {
  console.log('claimRewards obj', obj)
  return await broadcast(key, [['claim_reward_balance', obj]]);
};

export const broadcast = async (key, arr) => {
  const tx = new blurtTx.Transaction();
  console.log('tx bef', tx)
  await tx.create(arr);
  console.log('tx aft', tx)
  console.log('tx details', tx.transaction.operations)

  try {
    tx.sign(blurtTx.PrivateKey.from(key));
    const {error, result} = await tx.broadcast();

    if (error) {
      console.log(error);
      throw error;
    } else {
      return result;
    }
  } catch (e) {
    console.log('blurt-tx error', JSON.stringify(e));
    throw e;
  }

};

export default blurt;
