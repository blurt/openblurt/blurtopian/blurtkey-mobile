import {broadcastWithoutConfirmation} from 'components/browser/requestOperations/Broadcast';
import {broacastCustomJSONWithoutConfirmation} from 'components/browser/requestOperations/Custom';
import {decodeWithoutConfirmation} from 'components/browser/requestOperations/Decode';
import {encodeWithoutConfirmation} from 'components/browser/requestOperations/Encode';
import {postWithoutConfirmation} from 'components/browser/requestOperations/Post';
import {signBufferWithoutConfirmation} from 'components/browser/requestOperations/SignBuffer';
import {signTxWithoutConfirmation} from 'components/browser/requestOperations/SignTx';
import {voteWithoutConfirmation} from 'components/browser/requestOperations/Vote';

export const requestWithoutConfirmation = (
  accounts,
  request,
  sendResponse,
  sendError,
) => {
  switch (request.type) {
    case 'decode':
      request;
      decodeWithoutConfirmation(accounts, request, sendResponse, sendError);
      break;
    case 'signBuffer':
      request;
      signBufferWithoutConfirmation(accounts, request, sendResponse, sendError);
      break;
    case 'broadcast':
      request;
      broadcastWithoutConfirmation(accounts, request, sendResponse, sendError);
      break;
    case 'vote':
      request;
      voteWithoutConfirmation(accounts, request, sendResponse, sendError);
      break;
    case 'post':
      request;
      postWithoutConfirmation(accounts, request, sendResponse, sendError);
      break;
    case 'custom':
      request;
      broacastCustomJSONWithoutConfirmation(
        accounts,
        request,
        sendResponse,
        sendError,
      );
      break;
    case 'encode':
      request;
      encodeWithoutConfirmation(accounts, request, sendResponse, sendError);
      break;
    case 'signTx':
      request;
      signTxWithoutConfirmation(accounts, request, sendResponse, sendError);
      break;
  }
};
