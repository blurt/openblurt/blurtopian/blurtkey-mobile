import {treatBASRequest} from 'actions/blurtAuthenticationService';
import {Linking} from 'react-native';
import isURL from 'validator/lib/isURL';
import {BASConfig} from './config';
import {goBack} from './navigation';

export default async (addTabIfNew) => {
  Linking.addEventListener('url', ({url}) => {
    if (url) {
      handleUrl(url);
    }
  });
  const initialUrl = await Linking.getInitialURL();
  if (initialUrl) {
    handleUrl(initialUrl);
  }
};

export const handleUrl = (url, qr = false) => {
  if (url.startsWith(BASConfig.protocol)) {
    if (url.startsWith(BASConfig.auth_req)) {
      const buf = Buffer.from(url.replace(BASConfig.auth_req, ''), 'base64');
      const data = JSON.parse(buf.toString());
      if (qr) {
        goBack();
      }
      store.dispatch(treatBASRequest(data));
    }
  } else if (url.startsWith('blurt://')) {
    if (qr) {
      goBack();
    }
    if (url.startsWith('blurt://sign/op/')) {
      const op = url.replace('blurt://sign/op/', '');
      const stringOp = Buffer.from(op, 'base64').toString();
      const opJson = JSON.parse(stringOp);
      processQRCodeOp(opJson);
    }
  } else if (isURL(url)) {
    if (qr) {
      goBack();
    }
    //@ts-ignore
    store.dispatch(addTabFromLinking(url));
  }
};

export const clearLinkingListeners = () => {
  Linking.removeAllListeners('url');
};
