import api from 'api/keychain';
import ionomy from 'api/ionomy';
import blurt_price from 'api/blurt_price';
import {toHP} from 'utils/format';

export const getPrice = async () => {
  return (await ionomy.get('/api/v1/public/market-summary?market=btc-blurt'));
};

export const getBittrexPrices = async () => {
  return (await api.get('/hive/v2/bittrex')).data;
};

export const getBlurtPrice = async () => {
  return (await blurt_price.get('/price_info')).data;
};

export const getAccountValue = (
  { balance, vesting_shares},
  { hive },
  props,
) => {
  return (
    (toHP(vesting_shares, props) + parseFloat(balance)) * hive.Usd
  ).toFixed(3);
};
