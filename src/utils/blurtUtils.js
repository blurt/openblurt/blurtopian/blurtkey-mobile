import {sanitizeAmount} from './hiveUtils';

export const calculateTxFee = ({operation, properties}) => {
  let bandwidthKbytesFee = parseFloat(sanitizeAmount(properties.chain.bandwidth_kbytes_fee));
  let operationFlatFee = parseFloat(sanitizeAmount(properties.chain.operation_flat_fee));
  let size = JSON.stringify(operation).replace(/[\[\]\,\"]/g, '').length;
  let bw_fee = Math.max(
      0.001,
      ((size / 1024) * bandwidthKbytesFee).toFixed(3)
  );
  let fee = (operationFlatFee + bw_fee).toFixed(3);
  return fee;
}