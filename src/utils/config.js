export const hiveEngine = {
  CHAIN_ID: 'ssc-mainnet-hive',
};

export const blurtConfig = {
  CREATE_ACCOUNT_URL: 'https://joinblurt.com/',
};

export const BASConfig = {
  protocol: 'bas://',
  auth_req: 'bas://auth_req/',
  socket: 'wss://blurt-auth.blurtkey.com',
};

export const BrowserConfig = {
  HOMEPAGE_URL: 'about:blank',
  FOOTER_HEIGHT: 40,
  HEADER_HEIGHT: 45,
  HomeTab: {
    categories: [
      {title: 'finance', color: '#2A5320', logo: ''},
      {title: 'social', color: '#215858', logo: ''},
      // {title: 'video', color: '#2A435C', logo: ''},
      {title: 'explorer', color: '#6A3434', logo: ''},
      {title: 'tool', color: '#353E3E', logo: ''},
    ],
    dApps: [
      {
        name: 'Blurt Wallet',
        description: 'Official wallet for Blurt',
        icon: 'https://blocks.blurtwallet.com/img/blurt-logo.294f8e1c.png',
        url: 'https://blurtwallet.com/@',
        appendUsername: true,
        categories: ['finance'],
      },
      {
        name: 'Blurt.blog',
        description: 'Community interface for Blurt',
        icon: 'https://blocks.blurtwallet.com/img/blurt-logo.294f8e1c.png',
        url: 'https://blurt.blog',
        categories: ['social'],
      },
      {
        name: 'Blurt Explorer',
        description: 'Official Block Explorer',
        icon: 'https://blocks.blurtwallet.com/img/blurt-logo.294f8e1c.png',
        url: 'https://ecosynthesizer.com/blurt/@',
        appendUsername: true,
        categories: ['explorer'],
      },
      {
        name: 'Ecosynthesizer Explorer',
        description: 'Block Explorer by @symbionts',
        icon: 'https://ecosynthesizer.com/view/img/ECS-LOGO-2021.png',
        url: 'https://ecosynthesizer.com/blurt/@',
        appendUsername: true,
        categories: ['explorer'],
      },
      {
        name: 'Blurt Now',
        description: 'Track & Analyze Your Blurt Blockchain Account',
        icon: 'https://blurtopian.com/assets/img/logo-blurtopian.png',
        url: 'https://blurt-now.com',
        categories: ['tool'],
      },
      {
        name: 'Blurt BI (Business Intelligence)',
        description: 'For Your Blurt Business Intelligence Needs',
        icon: 'https://blurtopian.com/assets/img/logo-blurtopian.png',
        url: 'https://bi.blurt-now.com/',
        categories: ['tool'],
      }
    ],
  },
};

export const KeychainConfig = {
  NO_USERNAME_TYPES: [
    'delegation',
    'witnessVote',
    'proxy',
    'custom',
    'signBuffer',
    'transfer',
  ],
};

export const HiveTxConfig = {
  node: 'https://rpc.blurt.world',
  chain_id: 'cd8d90f29ae273abec3eaa7731e25934c63eb654d55080caff2ebb7f5df6381f',
  address_prefix: 'BLT'
}