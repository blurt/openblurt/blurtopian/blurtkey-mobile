import React from 'react';
import Blurt from 'assets/wallet/icon_blurt.svg';
import Hbd from 'assets/wallet/icon_hbd.svg';
import Bp from 'assets/wallet/icon_hp.svg';

export const getCurrencyProperties = (currency, account) => {
  let color, value, logo;
  switch (currency) {
    case 'BLURT':
      color = '#A3112A';
      logo = <Blurt />;
      value = account ? account.balance : null;
      break;
    case 'BP':
      color = '#AC4F00';
      value = account ? account.vesting_shares : null;
      logo = <Bp />;
      break;
  }
  return {currency, color, value, logo};
};
