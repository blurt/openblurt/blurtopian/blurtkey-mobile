export const addPreferenceToState = (
  state,
  username,
  domain,
  request,
) => {
  const userPref = state.find((u) => u.username === username);
  if (!userPref) {
    return [
      ...state,
      {username, domains: [{domain, whitelisted_requests: [request]}]},
    ];
  } else {
    const domainPref = userPref.domains.find((d) => d.domain === domain);
    if (domainPref) {
      if (!domainPref.whitelisted_requests.find((r) => r === request))
        return [
          ...state.filter((u) => u.username !== username),
          {
            username,
            domains: [
              ...userPref.domains.filter((d) => d.domain !== domain),
              {
                domain,
                whitelisted_requests: [
                  ...domainPref.whitelisted_requests,
                  request,
                ],
              },
            ],
          },
        ];
    } else {
      return [
        ...state.filter((u) => u.username !== username),
        {
          username,
          domains: [
            ...userPref.domains,
            {domain, whitelisted_requests: [request]},
          ],
        },
      ];
    }
  }
  return state;
};

export const removePreferenceFromState = (
  state,
  username,
  domain,
  request,
) => {
  let copyState = [...state];
  const user = copyState.find((u) => u.username === username);
  if (!user) return state;
  const dom = user.domains.find((d) => d.domain === domain);
  if (!dom) return state;
  dom.whitelisted_requests = dom.whitelisted_requests.filter(
    (r) => r !== request,
  );
  if (!dom.whitelisted_requests.length) {
    user.domains = user.domains.filter((d) => d.domain !== domain);
    if (!user.domains.length) {
      copyState = copyState.filter((u) => u.username !== username);
    }
  }
  return copyState;
};

export const hasPreference = (
  preferences,
  username,
  domain,
  type,
) => {
  try {
    return preferences
      .find((p) => p.username === username)
      .domains.find((d) => d.domain === domain)
      .whitelisted_requests.includes(type);
  } catch (e) {
    console.log('catching', e);
    return false;
  }
};
