import createTransform from 'redux-persist/es/createTransform';

const rpcTransformer = createTransform(
  (inboundState) => inboundState,
  (outboundState, key) => {
    if (key === 'settings' && typeof outboundState.rpc === 'string') {
      return {
        ...outboundState,
        rpc: {uri: outboundState.rpc, testnet: false},
      };
    }

    return outboundState;
  },
  {whitelist: ['settings']},
);

const blurtAuthenticationServiceTransformer = createTransform(
  (inboundState, key, state) => {
    if (state?.auth?.mk) return inboundState;
    const sessions = inboundState.sessions
      .filter((e) => e.token && e.token.expiration > Date.now())
      .map((e) => {
        e.init = false;
        return e;
      });
    return {
      instances: inboundState.instances
        .filter((e) => !!sessions.find((session) => e.host === session.host))
        .map((e) => {
          e.init = false;
          delete e.server_key;
          delete e.connected;
          return e;
        }),
      sessions,
    };
  },
  (outboundState) => outboundState,
  {whitelist: ['blurt_authentication_service']},
);

export default [rpcTransformer, blurtAuthenticationServiceTransformer];
