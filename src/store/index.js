import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {persistStore, persistReducer} from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
import reducers from 'reducers';
import transforms from './transforms';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: [
    'lastAccount',
    'settings',
    'browser',
    'preferences',
    'blurt_authentication_service',
    'walletFilters',
  ],
  transforms,
};
const persistConfig2 = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['lastAccount', 'settings', 'browser', 'preferences'],
};

const persistedReducers = persistReducer(persistConfig, reducers);
const store = createStore(persistedReducers, applyMiddleware(thunk));
const persistedReducers2 = persistReducer(persistConfig2, reducers);
const store2 = createStore(persistedReducers2, applyMiddleware(thunk));

const persistor = persistStore(store);

const getSafeState = () => {
  const state = {...store.getState()};
  state.accounts.forEach((e) => delete e.keys);
  delete state.activeAccount.keys;
  delete state.auth;
  delete state.conversions;
  delete state.phishingAccounts;
  delete state.activeAccount.account;
  delete state._persist;
  for (const e in state) {
    //@ts-ignore
    delete state[e]._persist;
  }
  return state;
};

export {store, persistor, getSafeState};
