import {ADD_PREFERENCE, REMOVE_PREFERENCE} from './types';

export const addPreference = (
  username,
  domain,
  request,
) => {
  return {
    type: ADD_PREFERENCE,
    payload: {
      username,
      domain,
      request,
    },
  };
};

export const removePreference = (
  username,
  domain,
  request,
) => {
  return {
    type: REMOVE_PREFERENCE,
    payload: {
      username,
      domain,
      request,
    },
  };
};
