import {navigate} from 'utils/navigation';
import {
  ADD_BROWSER_TAB,
  ADD_TO_BROWSER_FAVORITES,
  ADD_TO_BROWSER_HISTORY,
  BROWSER_FOCUS,
  CLEAR_BROWSER_HISTORY,
  CLOSE_ALL_BROWSER_TABS,
  CLOSE_BROWSER_TAB,
  REMOVE_FROM_BROWSER_FAVORITES,
  CLEAR_BROWSER_FAVORITES,
  SET_ACTIVE_BROWSER_TAB,
  UPDATE_BROWSER_TAB,
  UPDATE_MANAGEMENT,
} from './types';

export const addToHistory = (history) => {
  const action = {
    type: ADD_TO_BROWSER_HISTORY,
    payload: {history},
  };
  return action;
};

export const clearHistory = () => {
  const action = {
    type: CLEAR_BROWSER_HISTORY,
  };
  return action;
};

export const addToFavorites = (page) => {
  const action = {
    type: ADD_TO_BROWSER_FAVORITES,
    payload: {favorite: page},
  };
  return action;
};

export const removeFromFavorites = (url) => {
  const action = {
    type: REMOVE_FROM_BROWSER_FAVORITES,
    payload: {url},
  };
  return action;
};

export const clearFavorites = () => {
  console.log('clearFavorites')
  const action = {
    type: CLEAR_BROWSER_FAVORITES,
  };
  return action;
};

export const addTabFromLinking = (url) => (
  dispatch,
  getState,
) => {
  const existingTab = getState().browser.tabs.find((t) => t.url === url);
  if (existingTab) {
    dispatch(changeTab(existingTab.id));
  } else {
    const id = Date.now();
    dispatch({
      type: ADD_BROWSER_TAB,
      payload: {url, id},
    });
    dispatch(changeTab(id));
  }
  dispatch(showManagementScreen(false));
  if (getState().auth.mk) {
    navigate('BrowserScreen');
  } else {
    dispatch(setBrowserFocus(true));
  }
};

export const setBrowserFocus = (shouldFocus) => {
  const action = {
    type: BROWSER_FOCUS,
    payload: {shouldFocus},
  };
  return action;
};

export const addTab = (url) => {
  const action = {
    type: ADD_BROWSER_TAB,
    payload: {url, id: Date.now()},
  };
  return action;
};

export const closeTab = (id) => {
  const action = {
    type: CLOSE_BROWSER_TAB,
    payload: {id},
  };
  return action;
};

export const closeAllTabs = () => {
  return {
    type: CLOSE_ALL_BROWSER_TABS,
  };
};

export const changeTab = (id) => {
  return {
    type: SET_ACTIVE_BROWSER_TAB,
    payload: {id},
  };
};

export const updateTab = (id, data) => {
  const action = {
    type: UPDATE_BROWSER_TAB,
    payload: {id, data},
  };
  return action;
};

export const showManagementScreen = (showManagement) => {
  const action = {
    type: UPDATE_MANAGEMENT,
    payload: {showManagement},
  };
  return action;
};
