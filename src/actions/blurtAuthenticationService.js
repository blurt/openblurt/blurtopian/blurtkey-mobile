
export const treatBASRequest = (data) => {
  data.auth_key = data.key;
  delete data.key;
  return {
    type: "HAS_REQUEST",
    payload: data,
  };
};

export const showHASInitRequestAsTreated = (host) => {
  return {
    type: 'HAS_REQUEST_TREATED',
    payload: host,
  };
};

export const addSessionToken = (uuid, token) => {
  return {type: 'HAS_ADD_TOKEN', payload: {uuid, token}};
};

export const addServerKey = (host, server_key) => {
  return {
    type: 'HAS_ADD_SERVER_KEY',
    payload: {host, server_key},
  };
};

export const updateInstanceConnectionStatus = (
  host,
  connected,
) => {
  return {
    type: 'HAS_UPDATE_INSTANCE_CONNECTION_STATUS',
    payload: {host, connected},
  };
};

export const clearHASState = () => {
  return {
    type: HAS_ActionsTypes.CLEAR,
  };
};

export const removeHASSession = (uuid) => {
  return {
    type: 'HAS_REMOVE_SESSION',
    payload: {uuid},
  };
};

export const addWhitelistedOperationToSession = (
  uuid,
  operation,
) => {
  return {
    type: 'HAS_ADD_WHITELISTED_OPERATION',
    payload: {uuid, operation},
  };
};
