import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import Browser from 'screens/Browser';
const Stack = createStackNavigator();

export default () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="BrowserScreen"
        component={Browser}
        options={{
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
};
