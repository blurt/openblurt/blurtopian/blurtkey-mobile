import React from 'react';
import {StyleSheet, useWindowDimensions, View} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import DrawerButton from 'components/ui/DrawerButton';

import Claim from 'components/operations/ClaimRewards';

import Wallet from 'screens/wallet/Main';
import Blurt from 'assets/wallet/blurt.svg';

import {translate} from 'utils/localize';

const Stack = createStackNavigator();

export default () => {
  const {height, width} = useWindowDimensions();
  const styles = getStyles({width});

  return (
    <Stack.Navigator>
      <Stack.Screen
        name="WalletScreen"
        component={Wallet}
        options={({navigation}) => ({
          headerStyle: {
            backgroundColor: '#A3112A',
          },
          headerTitleAlign: 'center',
          title: translate('navigation.wallet'),
          headerTintColor: 'white',
          headerRight: () => (
            <View style={styles.containerRight}>
              <Claim />
              <DrawerButton navigation={navigation} />
            </View>
          ),

          headerLeft: () => {
            return <Blurt style={styles.left} />;
          },
        })}
      />
    </Stack.Navigator>
  );
};


const getStyles = ({width}) =>
  StyleSheet.create({
    left: {marginHorizontal: 0.05 * width},
    containerRight: {flexDirection: 'row'},
    qr: {marginLeft: 12},
  });