import React from 'react';
import {StyleSheet, Text, useWindowDimensions, Linking} from 'react-native';
import SafariView from 'react-native-safari-view';
import Background from 'components/ui/Background';
import GradientEllipse from 'components/ui/GradientEllipse';
import KeychainLogo from 'assets/bk_blurt.svg';
import Separator from 'components/ui/Separator';
import EllipticButton from 'components/form/EllipticButton';
import {translate} from 'utils/localize';
import {hiveConfig} from 'utils/config';

const Introduction = ({navigation}) => {
  const {height, width} = useWindowDimensions();
  const styles = getDimensionedStyles({height, width});
  return (
    <Background>
      <Separator height={height / 50} />
      <KeychainLogo {...styles.image} />
      <Separator height={height / 50} />
      <GradientEllipse style={styles.gradient} dotColor="red">
        <Text style={styles.text}>{translate('intro.text')}</Text>
      </GradientEllipse>
      <GradientEllipse style={styles.gradient} dotColor="white">
        <Text style={styles.text}>{translate('intro.manage')}</Text>
      </GradientEllipse>
      <Separator height={height / 20} />
      <EllipticButton
        title={translate('intro.existingAccount')}
        onPress={() => {
          navigation.navigate('SignupScreen');
        }}
      />
      <Separator height={height / 30} />
      <EllipticButton
        title={translate('intro.createAccount')}
        onPress={() => {
          SafariView.isAvailable()
            .then(available => {
              if (available) {
                SafariView.show({
                  url: blurtConfig.CREATE_ACCOUNT_URL
                });
              } else {
                Linking.canOpenURL(blurtConfig.CREATE_ACCOUNT_URL).then(
                  (supported) => {
                    if (supported) {
                      Linking.openURL(blurtConfig.CREATE_ACCOUNT_URL);
                    }
                  },
                );
              }
            })
            .catch(error => {
              // Fallback WebView code for iOS 8 and earlier
              Linking.canOpenURL(blurtConfig.CREATE_ACCOUNT_URL).then(
                (supported) => {
                  if (supported) {
                    Linking.openURL(blurtConfig.CREATE_ACCOUNT_URL);
                  }
                },
              );
            });
        }}
      />
    </Background>
  );
};

const getDimensionedStyles = ({width, height}) =>
  StyleSheet.create({
    image: {
      width: '90%',
      paddingHorizontal: width * 0.5,
    },
    gradient: {height: height / 10, marginTop: height / 20},
    text: {
      color: 'white',
      marginHorizontal: width * 0.05,
      fontSize: 15,
      textAlign: 'justify',
      flex: 1,
    },
  });

export default Introduction;
