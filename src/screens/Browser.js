import {
  addTab,
  addToFavorites,
  addToHistory,
  changeTab,
  clearHistory,
  closeAllTabs,
  closeTab,
  removeFromFavorites,
  clearFavorites,
  setBrowserFocus,
  showManagementScreen,
  updateTab,
} from 'actions/index';
import Browser from 'components/browser';
import React from 'react';
import Orientation from 'react-native-orientation-locker';
import {connect} from 'react-redux';

const BrowserScreen = ({
  accounts,
  browser,
  changeTab,
  addTab,
  updateTab,
  closeTab,
  closeAllTabs,
  addToHistory,
  clearHistory,
  addToFavorites,
  removeFromFavorites,
  clearFavorites,
  setBrowserFocus,
  navigation,
  route,
  showManagementScreen,
  preferences,
}) => {
  React.useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      Orientation.getAutoRotateState((s) => {
        if (s) {
          Orientation.unlockAllOrientations();
        } else console.log('cant rotate');
      });
    });

    return unsubscribe;
  }, [navigation]);

  return (
    <Browser
      accounts={accounts}
      navigation={navigation}
      route={route}
      browser={browser}
      changeTab={changeTab}
      addTab={addTab}
      updateTab={updateTab}
      closeTab={closeTab}
      closeAllTabs={closeAllTabs}
      addToHistory={addToHistory}
      clearHistory={clearHistory}
      addToFavorites={addToFavorites}
      removeFromFavorites={removeFromFavorites}
      clearFavorites={clearFavorites}
      setBrowserFocus={setBrowserFocus}
      showManagementScreen={showManagementScreen}
      preferences={preferences}
    />
  );
};

const mapStateToProps = (state) => {
  return {
    accounts: state.accounts,
    browser: state.browser,
    preferences: state.preferences,
  };
};

const connector = connect(mapStateToProps, {
  changeTab,
  addTab,
  updateTab,
  closeTab,
  closeAllTabs,
  addToHistory,
  clearHistory,
  addToFavorites,
  removeFromFavorites,
  clearFavorites,
  setBrowserFocus,
  showManagementScreen,
});

export default connector(BrowserScreen);
