import {
  fetchPhishingAccounts,
  loadAccount,
  loadBlurtPrice,
  loadProperties,
} from 'actions/index';
import UserPicker from 'components/form/UserPicker';
import PercentageDisplay from 'components/hive/PercentageDisplay';
import TxFeeDisplay from 'components/hive/TxFeeDisplay';
import Transactions from 'components/hive/Transactions';
import ScreenToggle from 'components/ui/ScreenToggle';
import WalletPage from 'components/ui/WalletPage';
import useLockedPortrait from 'hooks/useLockedPortrait';
import React, {useEffect} from 'react';
import {StyleSheet, useWindowDimensions, View} from 'react-native';
import {connect} from 'react-redux';
import Primary from 'screens/wallet/Primary';
import {getVP, getVotingDollarsPerAccount} from 'utils/hiveUtils';
import {translate} from 'utils/localize';

const Main = ({
  loadAccount,
  loadProperties,
  loadBlurtPrice,
  fetchPhishingAccounts,
  user,
  properties,
  accounts,
  lastAccount,
  navigation,
}) => {
  const styles = getDimensionedStyles(useWindowDimensions());

  useEffect(() => {
    loadAccount(lastAccount || accounts[0].name);
    loadProperties();
    loadBlurtPrice();
    fetchPhishingAccounts();
  }, [
    loadAccount,
    accounts,
    loadProperties,
    loadBlurtPrice,
    fetchPhishingAccounts,
    lastAccount,
  ]);

  useLockedPortrait(navigation);

  if (!user) {
    return null;
  }
  return (
    <WalletPage>
      <>
        <UserPicker
          accounts={accounts.map((account) => account.name)}
          username={user.name}
          //addAccount={() => {
          //  navigation.navigate('AddAccountFromWalletScreen', {wallet: true});
          //}}
          onAccountSelected={loadAccount}
        />
        <View style={styles.resourcesWrapper}>
          <TxFeeDisplay
            name={translate('wallet.flat_fee')}
            fee={(properties && properties.chain) ? properties.chain.operation_flat_fee : 0}
            secondaryName={translate('wallet.kbytes_fee')}
            secondaryFee={(properties && properties.chain) ? properties.chain.bandwidth_kbytes_fee : 0}
            color="#E59D15"
          />
          <PercentageDisplay
            name={translate('wallet.vp')}
            percent={getVP(user.account) || 100}
            color="#3BB26E"
          />
        </View>
        <ScreenToggle
          style={styles.toggle}
          menu={[
            translate(`wallet.menu.blurt`),
            translate(`wallet.menu.history`)
          ]}
          toUpperCase
          components={[<Primary />, <Transactions user={user} />]}
        />
      </>
    </WalletPage>
  );
};

const getDimensionedStyles = ({width, height}) =>
  StyleSheet.create({
    textCentered: {textAlign: 'center'},
    white: {color: 'white'},
    resourcesWrapper: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingLeft: width * 0.05,
      paddingRight: width * 0.05,
    },
    toggle: {
      display: 'flex',
      flexDirection: 'row',
      paddingLeft: width * 0.05,
      paddingRight: width * 0.05,
    },
  });
const connector = connect(
  (state) => {
    return {
      user: state.activeAccount,
      properties: state.properties,
      accounts: state.accounts,
      lastAccount: state.lastAccount.name,
    };
  },
  {
    loadAccount,
    loadProperties,
    loadBlurtPrice,
    fetchPhishingAccounts,
  },
);
export default connector(Main);
