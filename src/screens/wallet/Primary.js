import React from 'react';
import {View, StyleSheet, useWindowDimensions} from 'react-native';
import {connect} from 'react-redux';

import {toHP} from 'utils/format';
import {translate} from 'utils/localize';

import TokenDisplay from 'components/hive/TokenDisplay';
import Separator from 'components/ui/Separator';
import {
  Send,
  SendDelegation,
  SendPowerUp,
  SendPowerDown,
  SendDeposit,
  SendWithdraw,
} from 'components/operations/OperationsButtons';

import Blurt from 'assets/wallet/icon_blurt.svg';
import Hp from 'assets/wallet/icon_hp.svg';
import Savings from 'assets/wallet/icon_savings.svg';

const Primary = ({user, blurtPrice, properties}) => {
  const {width} = useWindowDimensions();

  return (
    <View style={styles.container}>
      <Separator height={20} />
      <TokenDisplay
        color="#A3112A"
        name="BLURT"
        currency="BLURT"
        value={parseFloat(user.account.balance)}
        logo={<Blurt width={width / 15} />}
        price={blurtPrice.price_usd}
        buttons={[
          <Send key="send_blurt" currency="BLURT" />,
          <SendPowerUp key="pu" />,
        ]}
      />
      <Separator height={20} />
      <TokenDisplay
        color="#AC4F00"
        name="BLURT POWER"
        currency="BP"
        value={toHP(user.account.vesting_shares, properties.globals)}
        incoming={toHP(
          user.account.received_vesting_shares,
          properties.globals,
        )}
        outgoing={toHP(
          user.account.delegated_vesting_shares,
          properties.globals,
        )}
        logo={<Hp width={width / 15} />}
        price={blurtPrice.price_usd}
        buttons={[<SendDelegation key="del" />, <SendPowerDown key="pd" />]}
      />
      <Separator height={20} />
      <TokenDisplay
        color="#7E8C9A"
        name={translate('common.savings').toUpperCase()}
        currency="BLURT"
        value={parseFloat(user.account.savings_balance)}
        logo={<Savings width={width / 15} />}
        buttons={[
          <SendWithdraw key="savings_withdraw" currency="BLURT" />,
          <SendDeposit key="savings_deposit" currency="BLURT" />,
        ]}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {width: '100%', flex: 1},
});

const mapStateToProps = (state) => {
  return {
    user: state.activeAccount,
    blurtPrice: state.blurtPrice,
    properties: state.properties,
  };
};

export default connect(mapStateToProps)(Primary);
