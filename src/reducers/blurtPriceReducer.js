import {GET_BLURT_PRICE} from 'actions/types';

const blurtPriceReducer = (
  state = {price_usd: {}, price_btc: {} },
  {type, payload},
) => {
  switch (type) {
    case GET_BLURT_PRICE:
      return payload;
    default:
      if (state.favorites) return state;
      return {...state, favorites: []};
  }
};

export default blurtPriceReducer;
