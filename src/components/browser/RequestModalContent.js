import Operation from 'components/operations/Operation';
import React from 'react';
import {translate} from 'utils/localize';
import {goBack} from 'utils/navigation';
import Requests from './requestOperations';
export default ({
  accounts,
  request,
  onForceCloseModal,
  sendResponse,
  sendError,
}) => {
  const renderOperationDetails = () => {
    const type = request.type;
    const Request = Requests[type];
    return (
      <Request
        request={request}
        accounts={accounts}
        sendResponse={sendResponse}
        sendError={sendError}
        closeGracefully={() => {
          goBack();
        }}
      />
    );
  };
  const getOperationTitle = (req) => {
    if (req.type === 'signBuffer' && req.title) {
      return req.title;
    }
    return translate(`request.title.${req.type}`);
  };
  console.log('RequestModalContent rerequestq', request)
  //TODO : add dApp icon
  return (
    <Operation title={getOperationTitle(request)} onClose={onForceCloseModal}>
      {renderOperationDetails()}
    </Operation>
  );
};
