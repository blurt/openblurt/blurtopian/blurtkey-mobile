import React, {MutableRefObject, useRef, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {WebView} from 'react-native-webview';
import {urlTransformer} from 'utils/browser';
import {BrowserConfig} from 'utils/config';
import {
  getRequiredWifType,
  sendError,
  sendResponse,
  validateAuthority,
  validateRequest,
} from 'utils/keychain';
import {goBack as navigationGoBack, navigate} from 'utils/navigation';
import {hasPreference} from 'utils/preferences';
import {requestWithoutConfirmation} from 'utils/requestWithoutConfirmation';
import {clearHistory, clearFavorites} from '../../actions/browser';
import {blurt_keychain} from './bridges/BlurtKeychainBridge';
import {BRIDGE_WV_INFO} from './bridges/WebviewInfo';
import Footer from './Footer';
import HomeTab from './HomeTab';
import ProgressBar from './ProgressBar';
import RequestModalContent from './RequestModalContent';
import RequestErr from './requestOperations/components/RequestError';

export default ({
  data: {url, id, icon, name},
  active,
  updateTab,
  accounts,
  navigation,
  addToHistory,
  history,
  manageTabs,
  isManagingTab,
  preferences,
  favorites,
  addTab,
  tabsNumber,
  orientation,
}) => {
  const tabData = {url, id, icon, name};
  const tabRef = useRef(null);
  const homeRef = useRef(null);
  const [canGoBack, setCanGoBack] = useState(false);
  const [canGoForward, setCanGoForward] = useState(false);
  const [progress, setProgress] = useState(0);
  const insets = useSafeAreaInsets();
  const FOOTER_HEIGHT = BrowserConfig.FOOTER_HEIGHT + insets.bottom;
  const goBack = () => {
    if (!canGoBack) {
      return;
    }
    const {current} = tabRef;
    current && current.goBack();
  };
  const goForward = () => {
    if (!canGoForward) {
      return;
    }
    const {current} = tabRef;
    current && current.goForward();
  };

  const reload = () => {
    const {current} = tabRef;
    current && current.reload();
  };

  const onLoadStart = ({
    nativeEvent: {url},
  }) => {
    updateTab(id, {url});
  };
  const updateTabUrl = (link) => {
    updateTab(id, {url: link});
  };
  const onLoadProgress = ({nativeEvent: {progress}}) => {
    setProgress(progress === 1 ? 0 : progress);
  };

  const onLoadEnd = ({
    nativeEvent: {canGoBack, canGoForward, loading},
  }) => {
    const {current} = tabRef;
    setProgress(0);
    if (loading) {
      return;
    }
    setCanGoBack(canGoBack);
    setCanGoForward(canGoForward);
    if (current) {
      current.injectJavaScript(BRIDGE_WV_INFO);
    }
  };

  const onMessage = ({nativeEvent}) => {
    const {name, request_id, data} = JSON.parse(nativeEvent.data);
    const {current} = tabRef;
    switch (name) {
      case 'swHandshake_blurt':
        current.injectJavaScript(
          'window.blurt_keychain.onAnswerReceived("blurt_keychain_handshake")',
        );
        break;
      case 'swRequest_blurt':
        if (validateRequest(data)) {
          const validateAuth = validateAuthority(accounts, data);
          if (validateAuth.valid) {
            showOperationRequestModal(request_id, data);
          } else {
            sendError(tabRef, {
              error: 'user_cancel',
              message: 'Request was canceled by the user.',
              data,
              request_id,
            });
            navigate('ModalScreen', {
              name: `Operation_${data.type}`,
              modalContent: (
                <RequestErr
                  onClose={() => {
                    navigationGoBack();
                  }}
                  error={validateAuth.error}
                />
              ),
            });
          }
        } else {
          sendError(tabRef, {
            error: 'incomplete',
            message: 'Incomplete data or wrong format',
            data,
            request_id,
          });
        }
        break;
      case 'WV_INFO':
        const {icon, name, url} = data;
        if (urlTransformer(url).host !== urlTransformer(tabData.url).host) {
          break;
        }
        if (
          tabData.url !== 'about:blank' &&
          (icon !== tabData.icon ||
            name !== tabData.name ||
            url !== tabData.url)
        ) {
          navigation.setParams({icon});
          if (name && url && url !== 'chromewebdata') {
            addToHistory({icon, name, url});
          }
          updateTab(id, {url, name, icon});
        }
        break;
    }
  };

  const showOperationRequestModal = (request_id, data) => {
    const {username, domain, type} = data;
    console.log('showOperationRequestModal', data)
    if (
      getRequiredWifType(data) !== 'active' &&
      hasPreference(
        preferences,
        username,
        urlTransformer(domain).hostname,
        type,
      ) &&
      username
    ) {
      requestWithoutConfirmation(
        accounts,
        {...data, request_id},
        (obj) => {
          sendResponse(tabRef, obj);
        },
        (obj) => {
          sendError(tabRef, obj);
        },
      );
    } else {
      const onForceCloseModal = () => {
        navigationGoBack();
        sendError(tabRef, {
          error: 'user_cancel',
          message: 'Request was canceled by the user.',
          data,
          request_id,
        });
      };
      navigate('ModalScreen', {
        name: `Operation_${data.type}`,
        modalContent: (
          <RequestModalContent
            request={{...data, request_id}}
            accounts={accounts}
            onForceCloseModal={onForceCloseModal}
            sendError={(obj) => {
              sendError(tabRef, obj);
            }}
            sendResponse={(obj) => {
              sendResponse(tabRef, obj);
            }}
          />
        ),
        onForceCloseModal,
      });
    }
  };
  return (
    <View
      style={[styles.container, !active || isManagingTab ? styles.hide : null]}>
      <View style={{flexGrow: 1}}>
        <ProgressBar progress={progress} />

        {url === BrowserConfig.HOMEPAGE_URL ? (
          <HomeTab
            history={history}
            favorites={favorites}
            updateTabUrl={updateTabUrl}
            homeRef={homeRef}
            accounts={accounts}
            clearHistory={clearHistory}
            clearFavorites={clearFavorites}
          />
        ) : null}
        <View
          style={
            url === BrowserConfig.HOMEPAGE_URL ? styles.hide : styles.container
          }>
          <WebView
            ref={tabRef}
            source={{
              uri: url === BrowserConfig.HOMEPAGE_URL ? null : url,
            }}
            sharedCookiesEnabled
            injectedJavaScriptBeforeContentLoaded={blurt_keychain}
            onMessage={onMessage}
            javaScriptEnabled
            allowsInlineMediaPlayback
            onLoadEnd={onLoadEnd}
            onLoadStart={onLoadStart}
            onLoadProgress={onLoadProgress}
            pullToRefreshEnabled
            onError={(error) => {
              console.log('Error', error);
            }}
            onHttpError={(error) => {
              console.log('HttpError', error);
            }}
          />
        </View>
      </View>
      {active && orientation === 'PORTRAIT' && (
        <Footer
          canGoBack={canGoBack}
          canGoForward={canGoForward}
          goBack={goBack}
          goForward={goForward}
          reload={reload}
          addTab={addTab}
          manageTabs={() => {
            manageTabs(
              {url, id, icon},
              url === BrowserConfig.HOMEPAGE_URL ? homeRef : tabRef,
            );
          }}
          height={FOOTER_HEIGHT}
          tabs={tabsNumber}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {flex: 1, flexDirection: 'column'},
  hide: {flex: 0, opacity: 0, display: 'none', width: 0, height: 0},
});
