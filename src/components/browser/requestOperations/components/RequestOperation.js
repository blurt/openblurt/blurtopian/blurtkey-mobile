import {addPreference} from 'actions/preferences';
import {RadioButton} from 'components/form/CustomRadioGroup';
import OperationButton from 'components/form/EllipticButton';
import React, {useState} from 'react';
import {ScrollView, StyleSheet, View} from 'react-native';
import {connect} from 'react-redux';
import {urlTransformer} from 'utils/browser';
import {beautifyErrorMessage} from 'utils/keychain';
import {translate} from 'utils/localize';
import RequestMessage from './RequestMessage';
import RequestResultMessage from './RequestResultMessage';

const RequestOperation = ({
  closeGracefully,
  sendResponse,
  sendError,
  message,
  children,
  method,
  request,
  successMessage,
  errorMessage,
  performOperation,
  additionalData = {},
  beautifyError,
  addPreference,
  selectedUsername,
}) => {
  const {request_id, ...data} = request;
  const [loading, setLoading] = useState(false);
  const [resultMessage, setResultMessage] = useState(null);
  const [keep, setKeep] = useState(false);
  let {domain, type, username} = data;
  domain = urlTransformer(domain).hostname;
  const renderRequestSummary = () => (
    <ScrollView>
      <RequestMessage message={message} />
      {children}
      {method !== 'active' && type !== 'addAccount' ? (
        <View style={styles.keep}>
          <RadioButton
            selected={keep}
            data={translate('request.keep', {
              domain,
              username: username || selectedUsername,
              type,
            })}
            style={styles.radio}
            onSelect={() => {
              setKeep(!keep);
            }}
          />
        </View>
      ) : (
        <></>
      )}
      <OperationButton
        style={styles.button}
        title={translate('request.confirm')}
        isLoading={loading}
        onPress={async () => {
          setLoading(true);
          let msg;
          try {
            const result = await performOperation();
            msg = successMessage;
            const obj = {
              data,
              request_id,
              result,
              message: msg,
              ...additionalData,
            };
            if (selectedUsername) obj.data.username = selectedUsername;
            if (keep) addPreference(username, domain, type);
            sendResponse(obj);
          } catch (e) {
            if (!beautifyError) {
              if (typeof errorMessage === 'function') {
                msg = errorMessage(e, data);
              } else {
                msg = errorMessage;
              }
            } else {
              msg = beautifyErrorMessage(e);
            }
            sendError({data, request_id, error: {}, message: msg});
          } finally {
            setResultMessage(msg);
          }
          setLoading(false);
        }}
      />
    </ScrollView>
  );

  if (resultMessage) {
    return (
      <RequestResultMessage
        closeGracefully={closeGracefully}
        resultMessage={resultMessage}
      />
    );
  } else {
    return renderRequestSummary();
  }
};

const styles = StyleSheet.create({
  button: {marginTop: 40},
  keep: {marginTop: 40, flexDirection: 'row'},
  radio: {marginLeft: 0},
});
const connector = connect(null, {addPreference});
export default connector(RequestOperation);

// Without confirmation :

// signTx

export const processOperationWithoutConfirmation = async (
  performOperation,
  request,
  sendResponse,
  sendError,
  beautifyError,
  successMessage,
  errorMessage,
  additionalData,
) => {
  const {request_id, ...data} = request;
  try {
    const result = await performOperation();
    let msg = successMessage;
    const obj = {
      data,
      request_id,
      result,
      message: msg,
      ...additionalData,
    };
    sendResponse(obj);
  } catch (e) {
    //console.log(e);
    let msg;
    if (!beautifyError) {
      // if (typeof errorMessage === 'function') {
      //   msg = errorMessage(e, data);
      // } else {
      msg = errorMessage;
      //}
    } else {
      msg = beautifyErrorMessage(e);
    }
    //console.log(msg);
    sendError({data, request_id, error: {}, message: msg});
  }
};
