import usePotentiallyAnonymousRequest from 'hooks/usePotentiallyAnonymousRequest';
import React from 'react';
import {broadcastJson} from 'utils/blurt';
import {translate} from 'utils/localize';
import CollapsibleData from './components/CollapsibleData';
import RequestItem from './components/RequestItem';
import RequestOperation, {
  processOperationWithoutConfirmation,
} from './components/RequestOperation';

export default ({
  request,
  accounts,
  closeGracefully,
  sendResponse,
  sendError,
}) => {
  const {request_id, ...data} = request;
  const {display_msg, id, json, method} = data;
  const {
    getUsername,
    getAccountKey,
    RequestUsername,
  } = usePotentiallyAnonymousRequest(request, accounts);

  return (
    <RequestOperation
      message={display_msg}
      sendResponse={sendResponse}
      sendError={sendError}
      successMessage={translate('request.success.broadcast')}
      beautifyError
      method={method.toLowerCase()}
      request={request}
      selectedUsername={getUsername()}
      closeGracefully={closeGracefully}
      performOperation={async () => {
        return await broadcastJson(
          getAccountKey(),
          getUsername(),
          id,
          method === 'Active',
          json,
        );
      }}>
      <RequestUsername />
      <RequestItem title={translate('request.item.method')} content={method} />
      <CollapsibleData
        title={translate('request.item.data')}
        hidden={translate('request.item.hidden_data')}
        content={JSON.stringify({id, json: JSON.parse(json)}, undefined, 2)}
      />
    </RequestOperation>
  );
};

const performBroadcastJSONOperation = async (
  accounts,
  request,
) => {
  const {id, json, method, username} = request;

  return await broadcastJson(
    accounts.find((e) => e.name === username).keys[
      method.toLowerCase()
    ],
    username,
    id,
    method === 'Active',
    json,
  );
};

export const broacastCustomJSONWithoutConfirmation = (
  accounts,
  request,
  sendResponse,
  sendError,
) => {
  processOperationWithoutConfirmation(
    async () => await performBroadcastJSONOperation(accounts, request),
    request,
    sendResponse,
    sendError,
    true,
    translate('request.success.broadcast'),
  );
};
