import React from 'react';
import {
  FlatList,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {translate} from 'utils/localize';
import HistoryItem from '../urlModal/HistoryItem';

export default ({history, updateTabUrl}) => {
  if (history && history.length) {
    history = history.filter(e => e != null);
  }

  return (
    <View style={styles.container}>
      {history.length ? (
        <FlatList
          data={[...history].reverse()}
          keyExtractor={(item) => {
            return item.url;
          }}
          renderItem={({item}) => (
            <HistoryItem
              data={item}
              key={item.url}
              onSubmit={(e) => {
                updateTabUrl(e);
              }}
            />
          )}
        />
      ) : (
        <Text style={styles.text}>{translate('browser.home.nothing')}</Text>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    marginTop: 30,
    flex: 1,
  },
  text: {alignSelf: 'center'},
  clearHistory: {
    marginLeft: 20,
    marginBottom: 10,
    fontWeight: 'bold',
  },
});
