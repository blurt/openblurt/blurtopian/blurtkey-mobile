import React from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  useWindowDimensions,
} from 'react-native';
import {translate} from 'utils/localize';

const CategoryButton = ({category, setCategory}) => {
  const styles = getStyles(useWindowDimensions());

  return (
    <TouchableOpacity
      style={[styles.category, {backgroundColor: category.color}]}
      onPress={() => {
        setCategory(category.title);
      }}>
      <Text style={styles.text}>
        {translate(`browser.home.categories.${category.title}`)}
      </Text>
    </TouchableOpacity>
  );
};

const getStyles = ({width}) =>
  StyleSheet.create({
    category: {
      marginHorizontal: 0.05 * width,
      width: width * 0.9,
      height: 46,
      justifyContent: 'center',
      alignItems: 'center',
      marginVertical: 7,
      borderRadius: 10,
    },
    text: {color: 'white', textTransform: 'uppercase', fontSize: 20},
  });

export default CategoryButton;
