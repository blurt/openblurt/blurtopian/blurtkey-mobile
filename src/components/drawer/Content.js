import {
  DrawerContentScrollView,
  DrawerItem,
  DrawerItemList,
} from '@react-navigation/drawer';
import {lock} from 'actions/index';
import DrawerFooter from 'components/drawer/Footer';
import DrawerHeader from 'components/drawer/Header';
import React from 'react';
import {StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {translate} from 'utils/localize';

const HeaderContent = (props) => {
  const {user, lock, navigation} = props;
  return (
    <DrawerContentScrollView
      {...props}
      contentContainerStyle={styles.contentContainer}>
      <DrawerHeader username={user.name} />
      <DrawerItemList {...props} />
      <DrawerItem
        {...props}
        label={translate('navigation.log_out')}
        onPress={() => {
          lock();
          navigation.closeDrawer();
        }}
      />
      <DrawerFooter user={user} />
    </DrawerContentScrollView>
  );
};
const styles = StyleSheet.create({contentContainer: {height: '100%'}});
const mapStateToProps = (state) => ({
  user: state.activeAccount,
});

const connector = connect(mapStateToProps, {lock});

export default connector(HeaderContent);
