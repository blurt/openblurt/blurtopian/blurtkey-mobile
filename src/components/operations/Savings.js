import {loadAccount} from 'actions/index';
import Savings from 'assets/wallet/icon_savings.svg';
import ActiveOperationButton from 'components/form/ActiveOperationButton';
import CustomPicker from 'components/form/CustomPicker';
import OperationInput from 'components/form/OperationInput';
import Separator from 'components/ui/Separator';
import React, {useState} from 'react';
import {
  Keyboard,
  StyleSheet,
  Text,
  useWindowDimensions,
  View,
  Alert,
} from 'react-native';
import Toast from 'react-native-simple-toast';
import {connect, ConnectedProps} from 'react-redux';
import {depositToSavings, withdrawFromSavings} from 'utils/blurt';
import {calculateTxFee} from 'utils/blurtUtils';
import {getCurrencyProperties} from 'utils/hiveReact';
import {translate} from 'utils/localize';
import {goBack} from 'utils/navigation';
import Balance from './Balance';
import Operation from './Operation';
import SavingsBalance from './SavingsBalance';

const Convert = ({user, loadAccount, currency, operation, properties}) => {
  const [amount, setAmount] = useState('');
  const [loading, setLoading] = useState(false);
  const [currencyState, setCurrency] = useState(currency);

  const onSavings = async () => {
    setLoading(true);
    try {
      const operationData = getOperation(operation);
      if (operation === "deposit") {
        await depositToSavings(user.keys.active, operationData);
      } else {
        await withdrawFromSavings(user.keys.active, operationData);
      }
      loadAccount(user.account.name, true);
      goBack();
      if (operation === "deposit") {
        Toast.show(
          translate('toast.savings_deposit_success', {
            amount: `${(+amount).toFixed(3)} ${currencyState}`,
          }),
          Toast.LONG,
        );
      } else {
        Toast.show(
          translate('toast.savings_withdraw_success', {
            amount: `${(+amount).toFixed(3)} ${currencyState}`,
          }),
          Toast.LONG,
        );
      }
    } catch (e) {
      Toast.show(`Error : ${(e).message}`, Toast.LONG);
    } finally {
      setLoading(false);
    }
  };

  const getOperation = (operation) => {
    let operationData = {};
    if (operation === "deposit") {
      operationData = {
        request_id: Date.now(),
        from: user.name,
        to: user.name,
        amount: `${(+amount).toFixed(3)} ${currencyState}`,
        memo: '',
      }
    } else {
      operationData = {
        request_id: Date.now(),
        from: user.name,
        to: user.name,
        amount: `${(+amount).toFixed(3)} ${currencyState}`,
        memo: '',
      }
    }
    return operationData;
  }

  const confirmTxFeeDialog = async () => {
    Keyboard.dismiss();
    const operation = getOperation();
    let fee = calculateTxFee({operation, properties});

    Alert.alert(
      "Transaction Fee",
      `This operation will cost ${fee} BLURT.`,
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "OK",
          onPress: onSavings
        }
      ]
    );
  }
  const {color} = getCurrencyProperties(currencyState);
  const styles = getDimensionedStyles(color, useWindowDimensions());

  return (
    <Operation
      logo={<Savings />}
      title={translate(`wallet.operations.savings.${operation}`)}>
      <>
        <View style={styles.container}>
          <CustomPicker
            list={['BLURT']}
            selectedValue={currencyState}
            onSelected={setCurrency}
            prompt={translate('wallet.operations.savings.prompt')}
            style={styles.picker}
            dropdownIconColor="white"
            iosTextStyle={styles.iosPickerText}
          />
        </View>
        {operation === "deposit" ? (
          <Balance
            currency={currencyState}
            account={user.account}
            setMax={(value) => {
              setAmount(value);
            }}
          />
        ) : (
          <SavingsBalance
            currency={currencyState}
            account={user.account}
            setMax={(value) => {
              setAmount(value);
            }}
          />
        )}
        <Separator />
        <Text style={styles.disclaimer}>
          {translate(`wallet.operations.savings.disclaimer`, {currencyState})}
        </Text>
        <Separator />
        <OperationInput
          placeholder={'0.000'}
          keyboardType="numeric"
          rightIcon={<Text style={styles.currency}>{currencyState}</Text>}
          textAlign="right"
          value={amount}
          onChangeText={setAmount}
        />
        <Separator height={50} />
        <ActiveOperationButton
          title={translate(
            `wallet.operations.savings.${operation}_button`,
          ).toUpperCase()}
          onPress={confirmTxFeeDialog}
          style={styles.button}
          isLoading={loading}
        />
        <Separator />
      </>
    </Operation>
  );
};

const getDimensionedStyles = (color, {width, height}) =>
  StyleSheet.create({
    button: {backgroundColor: '#68A0B4'},
    currency: {fontWeight: 'bold', fontSize: 18, color},
    disclaimer: {textAlign: 'justify'},
    container: {
      display: 'flex',
      flexDirection: 'row',
      backgroundColor: '#7E8C9A',
      borderRadius: height / 30,
      marginVertical: height / 30,
      alignContent: 'center',
      justifyContent: 'center',
      minHeight: 50,
    },
    picker: {
      width: '80%',
      color: 'white',
      alignContent: 'center',
    },
    iosPickerText: {color: 'white'},
  });

const connector = connect(
  (state) => {
    return {
      user: state.activeAccount,
      properties: state.properties,
    };
  },
  {
    loadAccount,
  },
);
export default connector(Convert);
