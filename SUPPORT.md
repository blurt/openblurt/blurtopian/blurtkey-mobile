## SUPPORT

Blurtkey is maintained by @eastmael.

For issues in using the app, please reach out to me via discord.

* Discord: https://discord.gg/jvRKTdP5Hx
* Email: blurtopian@gmail.com
* Website: https://blurtopian.com/