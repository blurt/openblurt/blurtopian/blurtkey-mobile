# BlurtKey for Mobile

React Native wallet for the Blurt Blockchain

# Install

- `npm i`
- `cp android/gradle.properties.example android/gradle.properties`

# Run in dev environment

## Android:

`npm run android`

## iOS:

`npm run ios`

### References for errors encountered on first run

* run: npm install
* run: cd ios; pod install; cd../

https://stackoverflow.com/questions/44131862/react-native-run-ios-is-not-working

# Test production

## Android:

Bundle : `npm run android-bundle`
Test Production Release : `npm run android-release`

## iOS:

Coming soon

## Troubleshooting

- SHA1 error : `watchman watch-del-all && npm run clean`
